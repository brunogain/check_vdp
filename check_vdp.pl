#!/usr/bin/perl

# nagios: -epn

use DBI;
use Pod::Usage;
use Getopt::Long;
use DateTime;
use DateTime::Format::Pg;
use Data::Dumper;

use strict;
use warnings;

my %ERRORS=('OK'=>0,'WARNING'=>1,'CRITICAL'=>2,'UNKNOWN'=>3);
my $exit=0;

our $dbg=0;
my $server='';
my $host;
my $dbusername;
my $dbpassword;
my $check='vm';
my $tz='UTC';
my $dbname='mcdb';
my $dbport=5555;
my $warning=0;
my $critical=0;


GetOptions("debug" => \$dbg,
    "server:s" => \$server,
    "host=s" => \$host,
    "dbusername=s" => \$dbusername,
    "dbpassword=s" => \$dbpassword,
    "check:s" => \$check,
    "timezone:s" => \$tz,
    "dbname:s" => \$dbname,
    "dbport:i" => \$dbport,
    "warning:i" => \$warning,
    "critical:i" => \$critical) or pod2usage(255);

pod2usage(254) if ($check eq 'vm' && $server eq '');

my $dbh = DBI->connect("dbi:Pg:dbname=$dbname;host=$host;port=$dbport;", $dbusername, $dbpassword);
$dbh->do("SET TIME ZONE '$tz'");

if ($check eq 'vm')
{
    if ($warning==0 or $critical==0)
    {
        $warning=2;
        $critical=4;
    }
    my $sth = $dbh->prepare("SELECT display_name, started_ts at time zone 'UTC' as started_ts, completed_ts at time zone 'UTC' as completed_ts, snapup_number, status_code, error_code, status_code_summary, error_code_summary, group_name, completed_ts - started_ts as elapsedtime from v_activities 
    WHERE (cid,started_ts) IN (
    SELECT cid,
    MAX(started_ts)
    FROM v_activities
    WHERE type LIKE '%Snapup%'
    GROUP BY cid
    )
    AND display_name LIKE '$server'
    ");
    $sth->execute;
    debug($sth->rows);

    if ($sth->rows==1)
    {
        my $row = $sth->fetchrow_hashref;
        debug(Dumper($row));
        #check status first
        if ($row->{'error_code'} != 0 && $row->{'status_code'}!=30005)
        {
            print "Error found : ".$row->{'error_code_summary'}." (code ".$row->{'error_code'}.")\n";
            print "Last backup started at ".$row->{'started_ts'}." and finished at ".$row->{'completed_ts'}." (Elapsed: ".$row->{'elapsedtime'}.")\n";
            $exit=$ERRORS{'CRITICAL'};
        }
        else
        {
            if ($row->{'status_code'}==30000)
            {
                #check if warning or critical
                my $dt = DateTime::Format::Pg->parse_datetime($row->{'started_ts'} );
                my $dt2 = DateTime->now;
                my $days = int($dt2->subtract_datetime_absolute($dt)->seconds / 86400);
                debug($dt);
                if ($days < $warning)
                {
                    print "OK: ".$row->{'status_code_summary'}."\n";
                    print "Last backup started at ".$row->{'started_ts'}." and finished at ".$row->{'completed_ts'}." (Elapsed: ".$row->{'elapsedtime'}.")\n";
                    $exit=$ERRORS{'OK'};
                }
                elsif ($days >= $warning && $days < $critical)
                {
                    print "Warning: last backup started at ".$row->{'started_ts'}."\n";
                    $exit=$ERRORS{'WARNING'};
                }
                else
                {
                    print "Critical: last backup started at ".$row->{'started_ts'}."\n";
                    $exit=$ERRORS{'CRITICAL'};
                }
            }
            else
            {
                print "Warning: unknown error : ".$row->{'status_code_summary'}." (code ".$row->{'status_code'}.")\n";
                print "Last backup started at ".$row->{'started_ts'}." and finished at ".$row->{'completed_ts'}." (Elapsed: ".$row->{'elapsedtime'}.")\n";
                $exit=$ERRORS{'CRITICAL'};
            }
        }
        print "Backup job is ".$row->{'group_name'}."\n";
        
    }
    elsif ($sth->rows > 1)
    {
        print "Error, found ".$sth->rows." machines corresponding !\n";
        $exit=$ERRORS{'CRITICAL'};
    }
    else
    {
        print "Error, machine $server not found !\n";
        $exit=$ERRORS{'CRITICAL'};
    }
    $sth->finish;
}
elsif ($check eq 'diskspace')
{
    if ($warning==0 or $critical==0)
    {
        $warning=85;
        $critical=90;
    }
    my $sth = $dbh->prepare("SELECT date_time at time zone 'UTC' as date_time,AVG(utilization) as utilization FROM v_node_space
    WHERE (date_time,disk) IN ( SELECT max(date_time),disk FROM v_node_space GROUP BY disk )
    GROUP BY date_time");
    $sth->execute;
    my $row = $sth->fetchrow_hashref;
    debug("Int utilization : ".int($row->{'utilization'}));
    debug($warning." ".$critical);
    if (int($row->{'utilization'})>=$warning && int($row->{'utilization'})<$critical)
    {
        print "Warning: Disk space low, ".sprintf("%.2f",$row->{'utilization'})."% used (at ".$row->{'date_time'}.")\n";
        $exit=$ERRORS{'WARNING'};
    }
    elsif (int($row->{'utilization'})>=$critical)
    {
        print "Critical: Disk space very low, ".sprintf("%.2f",$row->{'utilization'})."% used (at ".$row->{'date_time'}.")\n";
        $exit=$ERRORS{'CRITICAL'};
    }
    else
    {
        print "OK: Disk space. ".sprintf("%.2f",$row->{'utilization'})."% used (at ".$row->{'date_time'}.")\n";
        $exit=$ERRORS{'OK'};
    }
    $sth->finish;
}

$dbh->disconnect;
exit($exit);

sub debug
{
    if ($dbg)
    {
        print "DBG: ".shift."\n";
    }
}

__END__

=head1 NAME

check_vdp.pl - Using Guide

=head1 SYNOPSIS

check_vdp.pl --host SERVER_VDP_IP --dbusername USER_FOR_DB --dbpassword PASSWORD_FOR_DB

 Options :
    --debug : to enable debugging
    --check : type of check, vm or diskspace (default: vm)
    --server : servername of vm to check (obligatory if check vm)
    --timezone TZ : to have time in your timezone (default: UTC)
    --dbname DBNAME : database name (default: mcdb)
    --dbport 1234 : port number for database (default: 5555)
    --warning 2 : for vm number of days for warning (default: 2)
                  for diskspace percent of full disk (default: 85)
    --critical 5 : for vm number of days for critical (default: 4)
                   for diskspace percent of full disk (default: 90)

=head1 DESCRIPTION

B<This program> will check the vmware data protection status of a virutal machine and space disks

=cut
