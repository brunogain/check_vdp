# Check vSphere Data Protection
===============================


## Introduction

As the product can only send mail, I have searched a way to monitor and integrate it in nagios.
There is some "hack" to the product, so it couls be broken by an update or you can broke it if you don't know what you are doing.


## Requirements

-  Nagios or monitoring software
-  Perl with theses modules
    -  DBI::Pg;
    -  Pod::Usage;
    -  Getopt::Long;
    -  DateTime;
    -  DateTime::Format::Pg;
    -  Data::Dumper;


## Installation

-  Connect to the appliance with ssh (login: root)
-  Edit file ''/etc/firewall.default'' and add att line 8 :

        $IPT -A INPUT -p tcp --dport 5555 -j ACCEPT

-  Edit ''/usr/local/avamar/var/mc/server_data/postgres/data/postgresql.conf'' and add this line to allow database to listen on network :

        listen_addresses = '*'

-  Become the admin user to connect to dabase :

        su - admin

-  Connect to database :

        psql -p 5555 mcdb

-  Create the user and gives rights :

        create user nagios with password 'StrongPassword';
        grant connect on database mcdb to nagios;
        grant select on v_activities to nagios;
        grant select on v_node_space to nagios;

-  Reboot the appliance
-  You can now use the script ''check_vdp.pl'' to monitor the virtual machine backup and check free space

## Use

        check_vdp.pl --host SERVER_VDP_IP --dbusername USER_FOR_DB --dbpassword PASSWORD_FOR_DB

Options :

    - --debug : to enable debugging
    - --check : type of check, vm or diskspace (default: vm)
    - --server : servername of vm to check (obligatory if check vm)
    - --timezone TZ : to have time in your timezone (default: UTC)
    - --dbname DBNAME : database name (default: mcdb)
    - --dbport 1234 : port number for database (default: 5555)
    - --warning 2 : for vm number of days for warning (default: 2)
                  for diskspace percent of full disk (default: 85)
    - --critical 5 : for vm number of days for critical (default: 4)
                   for diskspace percent of full disk (default: 90)

